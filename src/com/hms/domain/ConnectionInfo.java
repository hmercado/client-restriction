package com.hms.domain;

/**
 * Created by Hugo on 4/07/2016.
 */
public class ConnectionInfo {

    private int rtmpConnections;
    private int rtspConnections;
    private int hlsConnections;

    public void increaseRTMP() {
        rtmpConnections++;
    }

    public void decreaseRTMP() {
        if(rtmpConnections == 0) {
            return;
        }
        rtmpConnections--;
    }

    public void increaseRTSP() {
        rtspConnections++;
    }

    public void decreaseRTSP() {
        if(rtspConnections == 0) {
            return;
        }
        rtspConnections--;
    }

    public void increaseHLS() {
        hlsConnections++;
    }

    public void decreaseHLS() {
        if(hlsConnections == 0) {
            return;
        }
        hlsConnections--;
    }

    public int getAllConnections() {
        return rtmpConnections + rtspConnections + hlsConnections;
    }

    public int getRTMPConnections() {
        return rtmpConnections;
    }

    public int getRTSPConnections() {
        return rtspConnections;
    }

    public int getHLSConnections() {
        return hlsConnections;
    }
}
