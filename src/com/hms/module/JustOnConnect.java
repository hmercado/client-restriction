package com.hms.module;

import com.wowza.wms.amf.AMFDataList;
import com.wowza.wms.client.IClient;
import com.wowza.wms.request.RequestFunction;

/**
 * Created by Hugo on 1/06/2016.
 */
public interface JustOnConnect {

    void onConnect(IClient client, RequestFunction requestFunction, AMFDataList amfDataList);
}
