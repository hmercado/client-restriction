package com.hms.module;

import com.wowza.util.Base64;
import com.wowza.util.HTTPUtils;

import java.util.Map;

public class TokenHelper {

    private final static String USER_ID_PARAMETER_NAME = "userId";

    public static String getUserToken(String queryString) {
        Map<String, String> parametersMap = HTTPUtils.splitQueryStr(queryString);
        if(parametersMap == null || parametersMap.isEmpty()) {
            return null;
        }
        String input = parametersMap.get(USER_ID_PARAMETER_NAME);
        if(input == null || input.isEmpty()) {
            return null;
        }
        String decoded = webSafeBase64Decode(input);
        String[] data = decoded.split("\\|");
        if(data == null || data.length < 3) {
            return null;
        }
        return data[0];
    }

    public static String webSafeBase64Decode(String input) {
        input = input.replace('-', '+')
                .replace('_', '/')
                .replace('.', '=')
                .replace('*', '=');
        return new String(Base64.decode(input));
    }

}
