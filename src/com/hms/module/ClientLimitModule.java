package com.hms.module;

import com.hms.domain.ConnectionInfo;
import com.wowza.wms.amf.AMFDataList;
import com.wowza.wms.application.IApplicationInstance;
import com.wowza.wms.application.WMSProperties;
import com.wowza.wms.client.IClient;
import com.wowza.wms.httpstreamer.model.IHTTPStreamerSession;
import com.wowza.wms.module.IModuleOnApp;
import com.wowza.wms.module.IModuleOnConnect;
import com.wowza.wms.module.IModuleOnHTTPSession;
import com.wowza.wms.module.IModuleOnRTPSession;
import com.wowza.wms.module.ModuleBase;
import com.wowza.wms.request.RequestFunction;
import com.wowza.wms.rtp.model.RTPSession;

import java.util.HashMap;
import java.util.Map;

/**
 * Check if a client is trying to playback more than one time per IP or user id.
 */
public class ClientLimitModule  extends ModuleBase implements IModuleOnApp, IModuleOnConnect, IModuleOnHTTPSession, IModuleOnRTPSession {

    private IApplicationInstance applicationInstance;
    private static final String LIMIT_CLIENT_BY_IP_PROP = "LimitClientIp";
    private static final String LIMIT_CLIENT_BY_USERID_PROP = "LimitClientUserId";
    private static final String WHITE_LIST_PROP = "WhiteList";
    private Map<String, ConnectionInfo> userIpMap;
    private Map<String, ConnectionInfo> userIdMap;
    private String[] whiteList;
    private int limitByIp;
    private int limitByUserId;

    private enum PROTOCOLS {
        RTMP, RTSP, HLS
    }

    @Override
    public void onAppStart(IApplicationInstance applicationInstance) {
        this.applicationInstance = applicationInstance;
        WMSProperties props = applicationInstance.getProperties();
        this.limitByIp = props.getPropertyInt(LIMIT_CLIENT_BY_IP_PROP, 0);
        this.limitByUserId = props.getPropertyInt(LIMIT_CLIENT_BY_USERID_PROP, 0);
        String whiteListProp = props.getPropertyStr(WHITE_LIST_PROP, null);
        if(whiteListProp != null) {
            whiteList = whiteListProp.trim().split("\\s*,\\s*");
        }

        if(userIpMap == null) {
            userIpMap = new HashMap<>();
        } else {
            userIpMap.clear();
        }

        if(userIdMap == null) {
            userIdMap = new HashMap<>();
        } else {
            userIdMap.clear();
        }

        getLogger().info("ClientLimitModule#onAppStart: limit ip connections: " + limitByIp + " limit user id connections: " + limitByUserId);
    }

    @Override
    public void onAppStop(IApplicationInstance applicationInstance) {
        userIpMap.clear();
        userIdMap.clear();
    }

    @Override
    public void onConnect(IClient client, RequestFunction requestFunction, AMFDataList amfDataList) {
        String ipAddress = client.getIp();
        if(isNotRestricted(ipAddress)) {
            return;
        }

        String userId = null;
        if(limitByUserId > -1) {
            userId = TokenHelper.getUserToken(client.getQueryStr());
            if(userId == null || userId.isEmpty()) {
                client.shutdownClient();
                return;
            }
        }
        int userIpLimit = 0;
        int userIdLimit = 0;

        ConnectionInfo connectionInfoByIP = userIpMap.get(ipAddress);
        ConnectionInfo connectionInfoByID = userIdMap.get(userId);

        if(connectionInfoByIP != null) {
            userIpLimit = connectionInfoByIP.getAllConnections();
        }
        if(connectionInfoByID != null) {
            userIdLimit = connectionInfoByID.getAllConnections();
        }

        getLogger().info("User: " + userId + ", IP: " + ipAddress + ", Connections by ip: " + userIpLimit + ", Connection by user id: " + userIdLimit);

        if(limitByIp >= 0) {
            if(userIpLimit >= limitByIp) {
                getLogger().info(getLimitMessage(PROTOCOLS.RTMP, ipAddress, userId, connectionInfoByIP));
                client.shutdownClient();
                return;
            }
            if(connectionInfoByIP == null) {
                connectionInfoByIP = new ConnectionInfo();
                synchronized (userIpMap) {
                    userIpMap.put(ipAddress, connectionInfoByIP);
                }
            }
            connectionInfoByIP.increaseRTMP();
        }
        if(limitByUserId >= 0) {
            if(userIdLimit >= limitByUserId) {
                getLogger().info(getLimitMessage(PROTOCOLS.RTMP, ipAddress, userId, connectionInfoByID));
                client.shutdownClient();
                return;
            }
            if(connectionInfoByID == null) {
                connectionInfoByID = new ConnectionInfo();
                synchronized (userIdMap) {
                    userIdMap.put(userId, connectionInfoByID);
                }
            }
            connectionInfoByID.increaseRTMP();
            client.getProperties().put("userId", userId);
        }
    }
    
    @Override
	public void onDisconnect(IClient client) {
        String ipAddress = client.getIp();
        String userId = client.getProperties().getPropertyStr("userId");

        decreaseCount(ipAddress, userId);
	}

    public void onRTPSessionCreate(RTPSession rtpSession) {
        String ipAddress = rtpSession.getIp();
        if(isNotRestricted(ipAddress)) {
            return;
        }
        String userId = null;
        if(limitByUserId >= 0) {
            userId = TokenHelper.getUserToken(rtpSession.getQueryStr());
            if(userId == null || userId.isEmpty()) {
                rtpSession.rejectSession();
                return;
            }
        }
        int userIpLimit = 0;
        int userIdLimit = 0;

        ConnectionInfo connectionInfoByIP = userIpMap.get(ipAddress);
        ConnectionInfo connectionInfoByID = userIdMap.get(userId);

        if(connectionInfoByIP != null) {
            userIpLimit = connectionInfoByIP.getAllConnections();
        }
        if(connectionInfoByID != null) {
            userIdLimit = connectionInfoByID.getAllConnections();
        }

        if(limitByIp >= 0) {
            if(userIpLimit >= limitByIp) {
                getLogger().info(getLimitMessage(PROTOCOLS.RTSP, ipAddress, userId, connectionInfoByIP));
                rtpSession.rejectSession();
                return;
            }
            if(connectionInfoByIP == null) {
                connectionInfoByIP = new ConnectionInfo();
                synchronized (userIpMap) {
                    userIpMap.put(ipAddress, connectionInfoByIP);
                }
            }
            connectionInfoByIP.increaseRTSP();
        }
        if(limitByUserId >= 0) {
            if(userIdLimit >= limitByUserId) {
                getLogger().info(getLimitMessage(PROTOCOLS.RTMP, ipAddress, userId, connectionInfoByID));
                rtpSession.rejectSession();
                return;
            }
            if(connectionInfoByID == null) {
                connectionInfoByID = new ConnectionInfo();
                synchronized (userIdMap) {
                    userIdMap.put(userId, connectionInfoByID);
                }
            }
            connectionInfoByID.increaseRTSP();
            rtpSession.getProperties().put("userId", userId);
        }
    }

    public void onRTPSessionDestroy(RTPSession rtpSession) {
        String ipAddress = rtpSession.getIp();
        String userId = rtpSession.getProperties().getPropertyStr("userId");

        decreaseCount(ipAddress, userId);
    }

    @Override
    public void onHTTPSessionCreate(IHTTPStreamerSession streamerSession) {
        String ipAddress = streamerSession.getIpAddress();
        if(isNotRestricted(ipAddress)) {
            return;
        }
        String userId = null;
        if(limitByUserId >= 0) {
            userId = TokenHelper.getUserToken(streamerSession.getQueryStr());
            if(userId == null || userId.isEmpty()) {
                streamerSession.rejectSession();
                return;
            }
        }
        int userIpLimit = 0;
        int userIdLimit = 0;

        ConnectionInfo connectionInfoByIP = userIpMap.get(ipAddress);
        ConnectionInfo connectionInfoByID = userIdMap.get(userId);

        if(connectionInfoByIP != null) {
            userIpLimit = connectionInfoByIP.getAllConnections();
            getLogger().info(getInfoMessage(ipAddress, null, connectionInfoByIP));
        }
        if(connectionInfoByID != null) {
            userIdLimit = connectionInfoByID.getAllConnections();
            getLogger().info(getInfoMessage(null, userId, connectionInfoByID));
        }
        if(limitByIp >= 0) {
            if(userIpLimit >= limitByIp) {
                getLogger().info(getLimitMessage(PROTOCOLS.HLS, ipAddress, userId, connectionInfoByIP));
                streamerSession.rejectSession();
                return;
            }
            if(connectionInfoByIP == null) {
                connectionInfoByIP = new ConnectionInfo();
                synchronized (userIpMap) {
                    userIpMap.put(ipAddress, connectionInfoByIP);
                }
            }
            connectionInfoByIP.increaseRTSP();
        }
        if(limitByUserId >= 0) {
            if(userIdLimit >= limitByUserId) {
                getLogger().info(getLimitMessage(PROTOCOLS.HLS, ipAddress, userId, connectionInfoByID));
                connectionInfoByID.increaseHLS();
                return;
            }
            if(connectionInfoByID == null) {
                connectionInfoByID = new ConnectionInfo();
                synchronized (userIdMap) {
                    userIdMap.put(userId, connectionInfoByID);
                }
            }
            connectionInfoByID.increaseHLS();
            streamerSession.getProperties().put("userId", userId);
        }
    }

    @Override
    public void onHTTPSessionDestroy(IHTTPStreamerSession httpSession) {
        String ipAddress = httpSession.getIpAddress();
        String userId = httpSession.getProperties().getPropertyStr("userId");

        decreaseCount(ipAddress, userId);
    }



    private boolean isClientInWhiteList(String clientIp) {
        if(this.whiteList == null || this.whiteList.length == 0) {
            return false;
        }
        for(String ip : whiteList) {
            if(clientIp.equals(ip)) {
                return true;
            }
        }
        return false;
    }

    private String getInfoMessage(String ip, String userId, ConnectionInfo connectionInfo) {
        StringBuilder message = new StringBuilder();
        message.append("User current connection by ");
        if(ip != null) {
            message.append("IP(");
            message.append(ip);
            message.append("). ");
        }
        if(userId != null){
            message.append("User id(");
            message.append(userId);
            message.append("). ");
        }
        message.append("RTMP connections: ");
        message.append(connectionInfo.getRTMPConnections());
        message.append(", RTSP connections: ");
        message.append(connectionInfo.getRTSPConnections());
        message.append(", HLS connections: ");
        message.append(connectionInfo.getHLSConnections());
        return message.toString();
    }

    private String getLimitMessage(PROTOCOLS protocol, String ip, String userId, ConnectionInfo connectionInfo) {
        StringBuilder message = new StringBuilder();
        message.append("User has reached limit trying to play by ");
        message.append(protocol);
        message.append(". IP(");
        message.append(ip);
        message.append("). ");
        if(userId != null){
            message.append("User id(");
            message.append(userId);
            message.append("). ");
        }
        message.append("RTMP connections: ");
        message.append(connectionInfo.getRTMPConnections());
        message.append(", RTSP connections: ");
        message.append(connectionInfo.getRTSPConnections());
        message.append(", HLS connections: ");
        message.append(connectionInfo.getHLSConnections());
        return message.toString();
    }

	@Override
	public void onConnectAccept(IClient client) {
	}

	@Override
	public void onConnectReject(IClient arg0) {
	}

    private void decreaseCount(String ipAddress, String userId) {
        ConnectionInfo connectionInfoByIP = userIpMap.get(ipAddress);
        if(connectionInfoByIP != null) {
            connectionInfoByIP.decreaseRTMP();
        }
        ConnectionInfo connectionInfoByID = userIdMap.get(userId);
        if(connectionInfoByID != null) {
            connectionInfoByID.decreaseRTMP();
        }
    }

    private boolean isNotRestricted(String ipAddress) {
        return (limitByIp < 0 && limitByUserId < 0) || isClientInWhiteList(ipAddress);
    }
}
